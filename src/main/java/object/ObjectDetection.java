package object;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.dnn.Dnn;
import org.opencv.dnn.Net;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class ObjectDetection {

    static { System.load("C:/opencv/build/java/x64/opencv_java481.dll"); }

    private static List<String> classLabels;

    public static Net createModel(String frozenModelPath) {
        // Load the frozen model and configuration file
        Net net = Dnn.readNetFromTensorflow(frozenModelPath);

        // Check if the network is empty
        if (net.empty()) {
            System.err.println("Error: Could not load the neural network.");
            System.exit(-1);  // Exit the program
        }

        // You can also set other properties of the model here if needed

        return net;
    }

    public static void main(String[] args) {
        // Charger les fichiers
        String frozenModelPath = "C:\\Users\\Bastien\\Documents\\frozen_inference_graph.pb";

        // Charger le modèle
        Net detectionModel = createModel(frozenModelPath);

        // Charger l'image
        Mat img = Imgcodecs.imread("C:\\Users\\BASTIEN\\Documents\\voiture.jpg");
        
        String fileName = "C:\\\\Users\\\\Bastien\\\\Documents\\\\Labels.txt";
        String[] classLabels;

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String content = br.lines().reduce("", (line1, line2) -> line1 + line2);
            classLabels = content.split("\n");
            System.out.println(classLabels);
        } catch (IOException e) {
            e.printStackTrace();
            // Gérer l'exception en conséquence selon vos besoins.
        }
    }
}